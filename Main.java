package com.dovichousesimgame;

import java.lang;
import com.dovichousesimgame.PrevalenceGame;
import com.dovichousesimgame.KrebsvilleWorld;
import com.dovichousesimgame.Chicagolike;
import com.dovichousesimgame.Phagehexamernanocar2020Vehicle;
import com.dovichousesimgame.PlayerCharacter;
import com.dovichousesimgame.OwlhomosapienhumansAlly;
import com.dovichousesimgame.HouseOfkeggEnemy;
import com.dovichousesimgame.Explorer;
import com.dovichousesimgame.game.terrains.Chicagolike;
import com.dovichousesimgame.game.terrains.PrevalenceGame;
import com.dovichousesimgame.game.vehicles.Phagehexamernanocar2020Vehicle;

public class dovichousesimgameMain {
    public static void main(String[] args) {
        Maze mazeToExplore = new Maze();
        Maze aux = mazeToExplore.clone();
	    if (aux.findPathFrom(1, 0)) {
	        System.out.println("maze solved");
			aux.print();
			System.out.println("original maze");
			mazeToExplore.print();
			
		} else {
		    System.out.println("no solution found");
		}
    }
}
