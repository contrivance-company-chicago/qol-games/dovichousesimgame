public interface Explorer{
    public void explore(PrevalenceGame prevalencegameToExplore);
    public char getFootprint();
}
