package game.characters.enemies;
import game.characters.PlayerCharacter;
/**
 * This code allows you to represent a generic ghost to be used in a simple game.
 */
 
public class CyclothymiaGhost extends HouseOfkeggEnemy {
    public CylothymiaGhost(String name, String description){
		super(name, description, PlayerCharacter.CHAINS); 
		footprint = 'g';
	}
	
}
