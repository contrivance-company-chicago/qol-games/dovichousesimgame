package game.characters.enemies;
import game.characters.PlayerCharacter;
/**
 * This code allows you to represent a generic enemy to be used in a simple game.
 */

public class HouseOfkeggEnemy extends PlayerCharacter{
	public static final String DEFAULT_SCARY_MSG = "Do you want to know what the fear is?";
	
	public HouseOfkeggEnemy(String name, String description, int secretWeapon){
		super(name, description,secretWeapon);
	}
	public void scare(){
		System.out.println(DEFAULT_SCARY_MSG);
	}
	
	public void scare (String otherScaryMessage){
	    System.out.println(otherScaryMessage);
	}

}
